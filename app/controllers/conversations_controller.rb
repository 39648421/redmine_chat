class ConversationsController < ApplicationController
  layout false
  before_action :find_project_by_project_id, :only => [:create], :if => :group_message?

  def create
    if group_message?
      (render_403; return false) unless @project.module_enabled?(:redmine_chat) && User.current.allowed_to?(:launch_chat, @project)
      if Conversation.where(:project_id => @project.id).present?
        @conversation = Conversation.where(:project_id => @project.id).first
      else
        @conversation = Conversation.create!(conversation_params)
        @conversation.project_id = @project.id
      end
    else
      @sender = User.find(params[:sender_id])
      @recipient = User.find(params[:recipient_id])

      if Conversation.where("recipient_id = #{@recipient.id} AND sender_id = #{@sender.id}").present?
        @conversation = Conversation.where("recipient_id = #{@recipient.id} AND sender_id = #{@sender.id}").first
      elsif Conversation.where("recipient_id = #{@sender.id} AND sender_id = #{@recipient.id}").present?
        @conversation = Conversation.where("recipient_id = #{@sender.id} AND sender_id = #{@recipient.id}").first
      else
        @conversation = Conversation.create!(conversation_params)
        @conversation.sender_id = @sender.id
        @conversation.recipient_id = @recipient.id
      end
    end
    manual_flag = params[:manual]
    if manual_flag == "1"
      render json: { conversation_id: @conversation.id, manual: manual_flag }
    else 
      render json: { conversation_id: @conversation.id }
    end
  end

  def show
    @conversation = Conversation.find(params[:id])
    if group_message?
 #     @project = @conversation.project
  #    (render_403; return false) unless @project && @project.module_enabled?(:redmine_chat) && User.current.allowed_to?(:launch_chat, @project)
    end
    if (params[:manual] == "1")
      @manual_flag = true
    else
      @manual_flag = false
    end
    # get the setting's max number
    @limit = 5
   # if !ChatSetting[:max_number_in_chat, @project.id].blank?
    #  @limit = ChatSetting[:max_number_in_chat, @project].to_i
   # end
    @messages = ChatMessage.where(conversation: @conversation).limit(@limit).reorder("#{ChatMessage.table_name}.created_at DESC").to_a
    @messages.reverse!
    @message = ChatMessage.new
  end

  private
    def conversation_params
      params.permit(:sender_id, :recipient_id, :project_id)
    end
  
    def interlocutor(conversation)
      User.current == conversation.recipient ? conversation.sender : conversation.recipient
    end

    def group_message?
      return true if params[:project_id]
    end

end
