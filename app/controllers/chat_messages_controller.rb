class ChatMessagesController < ApplicationController
  before_action :set_chat_message, only: [:destroy]
  
  def create
    @conversation = Conversation.find(params[:conversation_id])
    if @conversation.project
      @project = @conversation.project  
      (render_403; return false) unless @project && @project.module_enabled?(:redmine_chat) && User.current.allowed_to?(:launch_chat, @project)
    end
    @message = @conversation.chat_messages.build(message_params)
    @message.user_id = User.current.id
    @message.save!

    if @conversation.project
      PrivatePub.publish_to "/chatroom", :conversation_id => @conversation.id, :project_id => @project.id
    else
      PrivatePub.publish_to "/chatroom", :conversation_id => @conversation.id
    end
    @path = conversation_path(@conversation)
  end
  
  def index
    @project = Project.find(params[:project_id])
    (render_403; return false) unless @project && @project.module_enabled?(:redmine_chat)
    @limit =  10
    @conversation = @project.conversation
    @chat_messages = ChatMessage.where(conversation: @conversation)
    @chat_messages_count = @chat_messages.count
    @chat_messages_pages = Paginator.new @chat_messages_count, @limit, params['page']
    @offset ||= @chat_messages_pages.offset
    @chat_messages = @chat_messages.order("#{ChatMessage.table_name}.created_at DESC").
                      limit(@limit).
                      offset(@offset).
                      to_a
    respond_to do |format|
      format.html {
        @chat_message = ChatMessage.new # for adding chat_message inline
        render :layout => false if request.xhr?
      }
      format.api
      format.atom { render_feed(@chat_messages, :title => (@project ? @project.name : Setting.app_title) + ": #{l(:label_chat_messages)}") }
    end
  end

  def destroy
    @project = Project.find(params[:project_id])
    if User.current.allowed_to?(:delete_chat_messages, @project) || (@chat_message.user == User.current && User.current.allowed_to?(:delete_own_chat_messages, @project))
      if @chat_message.destroy
        flash[:notice] = l(:notice_successful_delete)
      else
        flash[:error] = l(:notice_unsuccessful_save)
      end
      
      respond_to do |format|
        format.html { redirect_to :action => "index", :project_id => @chat_message.conversation.project }
        format.api  { head :ok }
      end
    else
      (render_403; return false)
    end
  end
  
  private
    def message_params
      params.require(:chat_message).permit(:body)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_chat_message
      @chat_message = ChatMessage.find(params[:id])
    end


end
