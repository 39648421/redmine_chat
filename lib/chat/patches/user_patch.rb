module RedmineChat
  module Patches
    module UserPatch
      def self.included(base) # :nodoc: 
        base.class_eval do    
          unloadable # Send unloadable so it will not be unloaded in development
          has_many :conversations, :foreign_key => :sender_id
        end  
      end  
    end
  end
end

unless Project.included_modules.include?(RedmineChat::Patches::UserPatch)
  Project.send(:include, RedmineChat::Patches::UserPatch)
end
