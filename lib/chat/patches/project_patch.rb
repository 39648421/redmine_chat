module RedmineChat
  module Patches
    module ProjectPatch
      def self.included(base) # :nodoc: 
        base.class_eval do    
          unloadable # Send unloadable so it will not be unloaded in development
          has_one :conversation, :dependent => :destroy 
        end  
      end  
    end
  end
end

unless Project.included_modules.include?(RedmineChat::Patches::ProjectPatch)
  Project.send(:include, RedmineChat::Patches::ProjectPatch)
end
