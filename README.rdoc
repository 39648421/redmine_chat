= redmine_chat

This is a simple redmine plugin for chat.

This plugin is open source and released under the terms of the GNU General Public License v2 (GPL).

= update

0.0.8

fixed the bug that twice triggered when click the min or max title icon in js

auto-pop message only when the project's message arrived 

0.0.7

message's body length limted in 1024

optimized the chat_user online status' process through rm_private_pub gem

0.0.6

mysql2 supported

adjusted the project module's name

the issue's number can be auto-linked

added the permissions for secure of the page accessing

fix the avater's shown in chat box 

bugs known:

  it sometimes doesn't work that minimizing or maximizing the chat box on my development enviroment. it will be fixed in the future
  
  when leave from the client(disconnected from faye channel) the online's recored maybe not be destroyed 

0.0.5

the default chat of auto-pop is first minimized unless it's mannual launched

0.0.4

optimised for the chat quick auto-pop feature when every page loading

it works in chrome with the max or min state but only open max in IE


0.0.3

extended the chat box to other main pages

0.0.2

began to custome the private_pub gem for redmine_chat plugin

finished the basic chat function
 

0.0.1

first init and propotype demo
